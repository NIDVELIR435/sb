import React from 'react';

export interface ButtonProps {
  text: string;
  type?: 'primary' | 'secondary' | 'link';
  onClick?: () => void;
  color: 'primary' | 'transparent' | 'gray',
}

export const Button: React.FC<ButtonProps> = ({
  text,
  type = 'primary',
  onClick,
  color='primary',
}) => {
  
  const buttonType = type === 'link' && 'hover:text-blue-700 text-gray-700';
  
  const buttonColor = color === 'transparent' ? 'bg-transparent border hover:bg-gray-200' : 'bg-gray-500';
  
  return (
    <button className={`px-4 py-2 rounded ${type ==='primary' && 'bg-blue-500 hover:bg-blue-600 text-sm text-white'} ${color !=='primary' && buttonColor}
    ${color !=='primary' && buttonType} `}
            onClick={onClick}
    >
      {text}
    </button>
  );
};