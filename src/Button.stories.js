import { Meta} from '@storybook/react';
import { Button } from './Button';
import { StoryObj } from "@storybook/react";

/** Button component */
const meta: Meta<typeof Button> = {
  component: Button,
  title: 'Button',
  args: {
    href: '',
  },
  tags: ['autodocs'],
  argTypes: {
    type: {
      control: {type: 'radio'},
      options: ['primary', 'secondary', 'link']
    }
  }
};
export default meta;

type Story = StoryObj<typeof meta>

/** Primary button*/
export const Primary: Story = {
  args: {
    type: 'primary',
    text: 'Click me!',
    color: 'primary',
  },
};

/** Outlined button*/
export const Outlined = {
  args: {
    type: 'link',
    text: 'Link me!',
    color: 'transparent',
  },
};

/** Secondary button*/
export const Secondary = {
  args: {
    type: 'secondary',
    text: 'I am a button!',
    color: 'gray',
  },
};